from django.urls import path
from . import views

urlpatterns = [
    path('Toko/', views.Toko, name='Toko'),
    path('Tokos/', views.Tokos, name='Tokos'),
]